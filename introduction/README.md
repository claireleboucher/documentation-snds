# Section introduction

Cette section herbergera un guide introductif au SNDS.

Ce guide pourra être assez détaillé, avec des chapitres et sous-chapitres, tout en conservant une structure linéaire. 

Les éléments spécifiques sont réservés à la section [Fiches thématiques](../fiches/README.md). 
