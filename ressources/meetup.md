# Meetup SNDS
<!-- SPDX-License-Identifier: MPL-2.0 -->

Ressource et présentations partagés durant les Meetup SNDS.

- [Chaîne youtube](https://www.youtube.com/channel/UCFmHnLZ31EgSrPhZQTM9weQ) du HealthDataHub
- [Dossier sur GitLab](https://gitlab.com/healthdatahub/documentation-snds/tree/master/files/presentations) contenant l'ensemble des présentations

# 2019.03.27 Meetup-SNDS1

[Page de l'événement](https://www.meetup.com/fr-FR/Health-Data-Hub/events/259764548/)

- Documentation ouverte du SNDS sur GitLab ([documentation-snds.health-data-hub.fr](https://documentation-snds.health-data-hub.fr/))
    - Présenté par Anne Cuerq de l'INDS et Pierre-Alain Jachiet de la DREES
    - Support de présentation
    [GSlides](https://docs.google.com/presentation/d/1WvC4879Exta3Iv5vT9p-aHdzEjl4vy4ElymcNYWDXCQ/edit#slide=id.g54f4ce99ab_0_5),
    [pdf](../files/presentations/meetup-snds1/2019.03.27_INDS_DREES_Documentation_SNDS_MPL-2.0.pdf),
    [enregistement](https://www.youtube.com/watch?v=JpUbZUWwEt4)
    
- Schéma formel et Dictionnaire interactif du SNDS ([dico-snds.health-data-hub.fr](http://dico-snds.health-data-hub.fr/))
    - Présenté par Matthieu Doutreligne et Viktor Jarry de la DREES
    - Support de présentation
    [GSlides](https://docs.google.com/presentation/d/1XMGUJKcxy1zqfII-EtQDbmR2tljj6A8NIyEiEKJmz-Q/edit#slide=id.g54533cc6ad_0_0), 
    [pdf](../files/presentations/meetup-snds1/2019.03.27_DREES_schema_dico_snds_MPL-2.0.pdf),
    [enregistement](https://www.youtube.com/watch?v=aAeAB301zzM)

- Appariements entre les cohortes et le SNDS - zoom sur un cas concret
    - Présenté par Mehdi Gabbas et Brice Dufresne de la CNAM
    - Support de présentation
     [pptx](../files/presentations/meetup-snds1/2019.03.27_CNAM_Possibilités_Appariement_SNDS_MPL-2.0.pptx) ,
    [pdf](../files/presentations/meetup-snds1/2019.03.27_CNAM_Possibilités_Appariement_SNDS_MPL-2.0.pdf),
    [enregistement](https://www.youtube.com/watch?v=8M12owyEst4)

# 2019.06.12 Meetup-SNDS2

[Page de l'événement](https://www.meetup.com/fr-FR/Health-Data-Hub/events/261637008/)

- 1990 - 2019 : du rêve à la réalité, le regard d'un clinicien sur le SNDS
  - Présenté par le Dr Bertrand Lukacs (APHP)
  - Support de présentation
    [ppt](../files/presentations/meetup-snds2/2019.06.12_Lukacs_SNDS-du-reve-a-la-realite_MPL-2.0.ppt) ,
    [pdf](../files/presentations/meetup-snds2/2019.06.12_Lukacs_SNDS-du-reve-a-la-realite_MPL-2.0.pdf),
    [enregistement à venir](https://www.youtube.com/watch?v=xAbyRQW8KlU)

- Application du Big Data sur les données du SNDS 
  - Introduit par Emmanuel Bacry
    - Directeur de recherche CNRS Université Paris Dauphine
    - Directeur scientifique de l’INDS
    - Responsable projets data-santé à Polytechnique
  - Présenté par Youcef Sebiat (Chef de projet data-santé à Polytechnique)
  - Support de présentation
    [GSlides](https://docs.google.com/presentation/d/1QhqVp8tJvf4hKPF8zqSz7rE9oGhTzYpuQQwH2iP46co/edit?usp=sharing),
    [pdf](../files/presentations/meetup-snds2/2019.06.12_CMAP-XDataInitiative-CNAM_Application-Big-Data-sur-le-SNDS_MPL-2.0.pdf),
    [enregistement à venir](https://www.youtube.com/watch?v=YsGsgD_NEUc)
  
- Temporalité de la restitution des données dans le SNDS – Synthèse pour les analyses longitudinales 
  - Présenté par Cedric Collin (Pharmacoépidemiologiste chez IQVIA)
  - Support de présentation
    [pptx](../files/presentations/meetup-snds2/2019.06.12_IQVIA_Temporalite-restitution-SNDS_MPL-2.0.pptx) ,
    [pdf](../files/presentations/meetup-snds2/2019.06.12_IQVIA_Temporalite-restitution-SNDS_MPL-2.0.pdf),
    [enregistement à venir](https://www.youtube.com/watch?v=-wGL753gCrI)
